#ifndef XPMS_H
#define XPMS_H

extern const char *const player_xpm[];
extern const char *const bullet_xpm[];
extern const char *const bomb_xpm[];
extern const char *const bonus_fire_xpm[];
extern const char *const bonus_shield_xpm[];
extern const char *const bonus_speed_xpm[];
extern const char *const bonus_multi_xpm[];
extern const char *const alien1_xpm[];
extern const char *const alien2_xpm[];
extern const char *const alien3_xpm[];
extern const char *const alien4_xpm[];
extern const char *const alien5_xpm[];
extern const char *const alien6_xpm[];
extern const char *const alien7_xpm[];
extern const char *const alien8_xpm[];
extern const char *const alien9_xpm[];
extern const char *const alien10_xpm[];
extern const char *const alien11_xpm[];
extern const char *const alien12_xpm[];
extern const char *const alien13_xpm[];
extern const char *const alien14_xpm[];

#endif
